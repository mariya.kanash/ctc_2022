package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func station(capacity int, s string, interval []int, wg *sync.WaitGroup, cash_channel chan int) {
	capacity_channel := make(chan int, capacity)
	auto_queue_capacity := 5
	var wg2 sync.WaitGroup
	for i := 0; i < auto_queue_capacity; i++ {
		capacity_channel <- 1
		wg2.Add(1)
		go func(n int) {
			car(s, interval)
			cash_channel <- 1
			cash(s, &wg2)
			<-cash_channel
			<-capacity_channel
		}(i)
	}
	wg2.Wait()
	wg.Done()
}

func cash(fuel_type string, wg *sync.WaitGroup) {
	rand_time := time.Duration(rand.Intn(2-1)+1) * time.Millisecond
	fmt.Println("Car with", fuel_type, "is on cash", rand_time)
	time.Sleep(rand_time)
	//fmt.Println("Car with", fuel, "is out cash")
	wg.Done()
}

func car(fuel string, interv []int) {
	rand_time := time.Duration(rand.Intn(interv[1]-interv[0])+interv[0]) * time.Millisecond
	fmt.Println("Car with", fuel, "is on station", rand_time)
	time.Sleep(rand_time)
	//fmt.Println("Car with", fuel, "is out if station")
}

func main() {
	cash_chan := make(chan int, 2)
	var wg sync.WaitGroup
	wg.Add(1)
	go station(4, "gas", []int{1, 5}, &wg, cash_chan)
	wg.Add(1)
	go station(4, "diesel", []int{1, 5}, &wg, cash_chan)
	wg.Add(1)
	go station(1, "LPG", []int{1, 5}, &wg, cash_chan)
	wg.Add(1)
	go station(8, "electricity", []int{3, 10}, &wg, cash_chan)
	wg.Wait()
}
