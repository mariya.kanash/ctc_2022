package cv01

import "fmt"

var num1, num2 = 0, 1

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	return func() int {
		num3 := num1 + num2
		num1 = num2
		num2 = num3
		return num3
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
