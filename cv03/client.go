package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

var url = "http://localhost:8080/"

func main() {
	getList()
	createProduct(5, "cookies", 55, 350)
	createProduct(3, "bread", 5, 4000)
	createProduct(4, "juice", 5, 7000)
	getList()
	updateProduct(4, "juice", 25, 950)
	getList()
	deleteProduct(4)
	getList()
	getProductById(5)
}

func createProduct(id int, name string, price int, amount int) {
	data := map[string]interface{}{"Id": id, "Name": name, "Price": price, "Amount": amount}
	jsonData, err := json.Marshal(data)

	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Post(url+"create", "application/json",
		bytes.NewBuffer(jsonData))

	defer resp.Body.Close()
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(responseData))
}

func updateProduct(id int, newName string, newPrice int, newAmount int) {
	data := map[string]interface{}{"Id": id, "Name": newName, "Price": newPrice, "Amount": newAmount}
	jsonData, err := json.Marshal(data)

	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Post(url+"update", "application/json",
		bytes.NewBuffer(jsonData))

	defer resp.Body.Close()
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(responseData))
}

func deleteProduct(id int) {
	data := map[string]interface{}{"Id": id}
	jsonData, err := json.Marshal(data)

	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Post(url+"delete", "application/json",
		bytes.NewBuffer(jsonData))

	defer resp.Body.Close()
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(responseData))
}

func getProductById(id int) {
	values := map[string]int{"Id": id}
	jsonData, err := json.Marshal(values)

	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post(url+"product", "application/json",
		bytes.NewBuffer(jsonData))

	defer resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)

	fmt.Println("Find by product id ", res)
}

func getList() {
	resp, err := http.Get(url + "products")
	if err != nil {
		panic(err)
	}

	// !!!
	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(responseData))
	_ = resp.Header
	_ = resp.Status
	_ = resp.ContentLength
	_ = resp.StatusCode
}
