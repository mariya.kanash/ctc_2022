package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/mariya.kanash/ctc_2022/cv05-07/cmd/client"
	"gitlab.com/mariya.kanash/ctc_2022/cv05-07/cmd/server"
	"gitlab.com/mariya.kanash/ctc_2022/cv05-07/pkg/util"
)

func main() {
	cmd := &cobra.Command{
		Use: "cv05-07",
		CompletionOptions: cobra.CompletionOptions{
			DisableDefaultCmd: true,
		},
	}

	cmd.AddCommand(server.Cmd(), client.Cmd())

	util.ExitOnError(cmd.Execute())
}
